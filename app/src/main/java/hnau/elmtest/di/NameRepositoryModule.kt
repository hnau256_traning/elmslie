package hnau.elmtest.di

import dagger.Module
import dagger.Provides
import hnau.elmtest.repository.NameRepository
import javax.inject.Singleton

@Module
class NameRepositoryModule {

    @Singleton
    @Provides
    fun provideNameRepository() = NameRepository()

}
