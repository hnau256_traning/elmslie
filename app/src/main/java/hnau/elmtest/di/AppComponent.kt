package hnau.elmtest.di

import dagger.Component
import hnau.elmtest.AppActivity
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        NameRepositoryModule::class
    ]
)
interface AppComponent {

    fun inject(target: AppActivity)

}
