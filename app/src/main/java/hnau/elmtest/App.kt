package hnau.elmtest

import android.app.Application
import hnau.elmtest.di.AppComponent
import hnau.elmtest.di.DaggerAppComponent

class App: Application() {

    init {
        component = DaggerAppComponent.create()
    }

    companion object {

        lateinit var component: AppComponent
            private set

    }

}
