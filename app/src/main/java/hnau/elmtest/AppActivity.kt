package hnau.elmtest

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import hnau.elmtest.repository.NameRepository
import hnau.elmtest.ui.AppView
import javax.inject.Inject

class AppActivity: AppCompatActivity() {

    @Inject
    lateinit var nameRepository: NameRepository

    init {
        App.component.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            AppView(
                nameRepository = nameRepository
            )
        }
    }
}
