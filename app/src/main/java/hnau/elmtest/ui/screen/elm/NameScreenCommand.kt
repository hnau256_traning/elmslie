package hnau.elmtest.ui.screen.elm


sealed interface NameScreenCommand {

    object LoadName: NameScreenCommand

}
