package hnau.elmtest.ui.screen.elm


sealed interface NameScreenEvent {

    sealed interface Internal: NameScreenEvent {

        data class OnSuccess(
            val name: String
        ): Internal

        data class OnError(
            val errorMessage: String
        ): Internal

    }

    sealed interface UI: NameScreenEvent {

        object Reload: UI

    }

}
