package hnau.elmtest.ui.screen.elm


data class NameScreenState(
    val isLoading: Boolean,
    val content: Content?
) {

    sealed interface Content {

        data class Success(
            val name: String
        ): Content

        data class Error(
            val errorMessage: String
        ): Content

    }

    companion object {

        val INITIAL = NameScreenState(
            isLoading = false,
            content = null
        )

    }

}
