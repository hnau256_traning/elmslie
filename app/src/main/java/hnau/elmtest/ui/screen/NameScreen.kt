package hnau.elmtest.ui.screen

import android.annotation.SuppressLint
import androidx.compose.animation.AnimatedContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.OutlinedButton
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import arrow.core.Option
import hnau.elmtest.R
import hnau.elmtest.repository.NameRepository
import hnau.elmtest.ui.screen.elm.NameScreenActor
import hnau.elmtest.ui.screen.elm.NameScreenCommand
import hnau.elmtest.ui.screen.elm.NameScreenEffect
import hnau.elmtest.ui.screen.elm.NameScreenEvent
import hnau.elmtest.ui.screen.elm.NameScreenReducer
import hnau.elmtest.ui.screen.elm.NameScreenState
import hnau.elmtest.ui.util.AnimatedVisibility
import hnau.elmtest.ui.util.elm.ElmScreen
import hnau.elmtest.ui.util.elm.StoreSnapshot
import vivid.money.elmslie.core.store.ElmStore

object NameScreen {

    const val key = "name"

    @Composable
    @SuppressLint("UnusedMaterialScaffoldPaddingParameter")
    fun Show(
        nameRepository: NameRepository
    ) = ElmScreen(
        storeFactory = {
            ElmStore<NameScreenEvent, NameScreenState, NameScreenEffect, NameScreenCommand>(
                initialState = NameScreenState.INITIAL,
                reducer = NameScreenReducer,
                startEvent = NameScreenEvent.UI.Reload,
                actor = NameScreenActor(
                    nameRepository = nameRepository
                )
            )
        }
    ) {
        val scaffoldState = rememberScaffoldState()
        LaunchedEffect(effects) {
            effects.collect { effect ->
                when (effect) {
                    is NameScreenEffect.ShowError -> scaffoldState.snackbarHostState.showSnackbar(
                        message = effect.errorMessage
                    )
                }
            }
        }
        Scaffold(
            scaffoldState = scaffoldState
        ) {
            Content()
        }
    }

    @Composable
    private fun StoreSnapshot<NameScreenEvent, NameScreenEffect, NameScreenState>.Content() = Box(
        contentAlignment = Alignment.Center
    ) {
        Column(
            modifier = Modifier
                .padding(16.dp)
                .fillMaxWidth(),
            verticalArrangement = Arrangement.spacedBy(16.dp)
        ) {
            Box(
                modifier = Modifier
                    .weight(1f)
                    .fillMaxWidth()
            ) {
                AnimatedVisibility(
                    value = state,
                    toLocal = { Option.fromNullable(content) },
                    modifier = Modifier.fillMaxSize()
                ) { content ->
                    AnimatedContent(
                        targetState = content,
                        modifier = Modifier.fillMaxSize()
                    ) { contentLocal ->
                        Box(
                            contentAlignment = Alignment.Center,
                            modifier = Modifier.fillMaxSize()
                        ) {
                            Text(
                                text = when (contentLocal) {
                                    is NameScreenState.Content.Error -> contentLocal.errorMessage
                                    is NameScreenState.Content.Success -> contentLocal.name
                                },
                                color = when (contentLocal) {
                                    is NameScreenState.Content.Error -> MaterialTheme.colors.error
                                    is NameScreenState.Content.Success -> MaterialTheme.colors.onBackground
                                },
                                modifier = Modifier.wrapContentSize(),
                                textAlign = TextAlign.Center,
                                style = MaterialTheme.typography.h5
                            )
                        }
                    }
                }
            }
            OutlinedButton(
                onClick = { accept(NameScreenEvent.UI.Reload) },
                enabled = !state.isLoading,
                modifier = Modifier.fillMaxWidth()
            ) {
                Text(stringResource(R.string.reload))
            }
        }

        androidx.compose.animation.AnimatedVisibility(
            visible = state.isLoading
        ) {
            CircularProgressIndicator()
        }
    }

}
