package hnau.elmtest.ui.screen.elm

import vivid.money.elmslie.core.store.dsl_reducer.DslReducer

object NameScreenReducer: DslReducer<NameScreenEvent, NameScreenState, NameScreenEffect, NameScreenCommand>() {

    override fun Result.reduce(
        event: NameScreenEvent
    ): Any? = when (event) {
        is NameScreenEvent.Internal.OnSuccess -> {
            state {
                copy(
                    isLoading = false,
                    content = NameScreenState.Content.Success(event.name)
                )
            }
        }
        is NameScreenEvent.Internal.OnError -> {
            effects { +NameScreenEffect.ShowError(event.errorMessage) }
            state {
                copy(
                    isLoading = false,
                    content = NameScreenState.Content.Error(event.errorMessage)
                )
            }
        }
        NameScreenEvent.UI.Reload -> {
            state { copy(isLoading = true) }
            commands { +NameScreenCommand.LoadName }
        }
    }

}
