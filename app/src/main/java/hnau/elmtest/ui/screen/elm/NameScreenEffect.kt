package hnau.elmtest.ui.screen.elm


sealed interface NameScreenEffect {

    data class ShowError(
        val errorMessage: String
    ): NameScreenEffect

}
