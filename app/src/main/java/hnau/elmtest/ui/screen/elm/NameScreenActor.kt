package hnau.elmtest.ui.screen.elm

import hnau.elmtest.repository.NameIsNotAvailableException
import hnau.elmtest.repository.NameRepository
import kotlinx.coroutines.flow.flow
import vivid.money.elmslie.core.store.DefaultActor

class NameScreenActor(
    private val nameRepository: NameRepository
): DefaultActor<NameScreenCommand, NameScreenEvent> {

    override fun execute(
        command: NameScreenCommand
    ) = when (command) {
        NameScreenCommand.LoadName -> flow<NameScreenEvent> {
            try {
                val name = nameRepository.getName()
                emit(NameScreenEvent.Internal.OnSuccess(name))
            } catch (ex: NameIsNotAvailableException) {
                emit(NameScreenEvent.Internal.OnError(ex.message ?: "Unknown error"))
            }
        }
    }

}
