package hnau.elmtest.ui.util.elm

import vivid.money.elmslie.core.store.Store

fun interface StoreFactory<Event, Effect, State> {

    fun createStore(): Store<Event, Effect, State>

}
