package hnau.elmtest.ui.util.elm

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.remember
import kotlinx.coroutines.flow.Flow

@Composable
fun <Event, Effect, State> ElmScreen(
    storeFactory: StoreFactory<Event, Effect, State>,
    content: @Composable StoreSnapshot<Event, Effect, State>.() -> Unit
) {
    val store = viewModelStore(storeFactory)
    val state = store.states().collectAsState(store.currentState)
    val eventsReceiver: StoreSnapshot<Event, Effect, State> = remember {
        object: StoreSnapshot<Event, Effect, State> {

            override val effects = store.effects()

            override val state: State get() = state.value

            override fun accept(event: Event) = store.accept(event)

        }
    }
    eventsReceiver.content()
}

interface StoreSnapshot<Event, Effect, State> {

    val state: State

    val effects: Flow<Effect>

    fun accept(event: Event)
}
