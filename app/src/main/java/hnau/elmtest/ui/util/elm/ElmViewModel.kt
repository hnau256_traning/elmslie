package hnau.elmtest.ui.util.elm

import androidx.compose.runtime.Composable
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewmodel.compose.viewModel
import vivid.money.elmslie.core.store.Store

@Composable
fun <Event, Effect, State> viewModelStore(
    storeFactory: StoreFactory<Event, Effect, State>
): Store<Event, Effect, State> = viewModel<ElmViewModel<Event, Effect, State>>(
    factory = object: ViewModelProvider.Factory {

        override fun <T: ViewModel> create(
            modelClass: Class<T>
        ) = ElmViewModel(
            storeFactory = storeFactory
        ) as T

    }
)

private class ElmViewModel<Event, Effect, State>(
    storeFactory: StoreFactory<Event, Effect, State>
): ViewModel(), Store<Event, Effect, State> by storeFactory.createStore() {

    init {
        start()
    }

    override fun onCleared() {
        super.onCleared()
        stop()
    }

}
