package hnau.elmtest.ui

import androidx.compose.runtime.Composable
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import hnau.elmtest.repository.NameRepository
import hnau.elmtest.ui.screen.NameScreen


@Composable
fun AppView(
    nameRepository: NameRepository
) = AppTheme {
    val navController = rememberNavController()
    NavHost(
        navController = navController,
        startDestination = NameScreen.key
    ) {
        composable(
            route = NameScreen.key
        ) {
            NameScreen.Show(
                nameRepository = nameRepository
            )
        }
    }
}
