package hnau.elmtest.repository

import kotlinx.coroutines.delay
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlin.time.Duration.Companion.seconds

class NameRepository {

    private var attempt = 0

    private val getNameMutex = Mutex()

    @Throws(NameIsNotAvailableException::class)
    suspend fun getName(): String = getNameMutex.withLock {
        delay(1.seconds)
        attempt++
        val attemptSuffix = " (Attempt $attempt)"
        if (attempt < FIRST_SUCCESS_ATTEMPT) {
            throw NameIsNotAvailableException("Name is not available" + attemptSuffix)
        } else {
            "Name" + attemptSuffix
        }
    }

    companion object {
        private const val FIRST_SUCCESS_ATTEMPT = 3
    }

}
