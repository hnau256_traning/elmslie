package hnau.elmtest.repository


class NameIsNotAvailableException(
    message: String
): IllegalStateException(
    message
)
